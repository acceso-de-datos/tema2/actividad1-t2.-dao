package vista;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import dao.ArticuloDAO;
import dao.ClienteDAO;
import dao.ConexionBD;
import dao.GrupoDAO;
import dao.VendedorDAO;
import modelo.Articulo;
import modelo.Cliente;
import modelo.Grupo;
import modelo.Vendedor;

// para corregir la actividad2
public class TestApp {
	final static String TUNOMBRE = "FABRICIO";

	public static void main(String[] args) {

		try {
			System.out.println("------------------------------------ CLIENTES ------------------------------------");
			testClienteDAO();
			System.out.println();

			System.out.println("------------------------------------------------------------------------------------");
			System.out.println("------------------------------------ VENDEDORES ------------------------------------");
			testVendedorDAO();
			System.out.println();

			System.out.println("--------------------------------------------------------------------------------");
			System.out.println("------------------------------------ GRUPOS ------------------------------------");
			testGrupoDAO();
			System.out.println();

			System.out.println("-----------------------------------------------------------------------------------");
			System.out.println("------------------------------------ ARTICULOS ------------------------------------");
			testArticuloDAO();

			ConexionBD.cerrar();

		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	private static void testArticuloDAO() throws SQLException {
		ArticuloDAO artdao = new ArticuloDAO();

		// insertar
		GrupoDAO gdao = new GrupoDAO();
		Grupo g = gdao.findByPK(1);
		Articulo a = new Articulo("articulo " + TUNOMBRE, 10, "artdao", g);
		if ((a = artdao.insertGenKey(a)) != null) {
			System.err.println("Insertado!! " + a);
		}

		// listar
		List<Articulo> articulos = artdao.findAll();
		for (Articulo art : articulos) {
			System.out.println(art);
		}

		// buscar byexample
		System.err.println("Listar por findbyexample");
		articulos = artdao.findByExample(new Articulo("Monitor", -1, null, null));
		for (Articulo art : articulos) {
			System.out.println(art);
		}
		System.out.println("--");
		articulos = artdao.findByExample(new Articulo(null, 202, null, null));
		for (Articulo art : articulos) {
			System.out.println(art);
		}
		
		// buscar por clave ajena
		System.out.println("--");
		g = gdao.findByPK(2);
		articulos = artdao.findArticulosOfGrupo(g);
		for (Articulo art : articulos) {
			System.out.println(art);
		}

		artdao.cerrar();
	}

	private static void testGrupoDAO() throws SQLException {
		GrupoDAO grudao = new GrupoDAO();

		// insertar
		Grupo g = new Grupo("grup " + TUNOMBRE);
		if ((g = grudao.insertGenKey(g)) != null) {
			System.err.println("Insertado!! " + g);
		}

		// listar
		List<Grupo> grupos = grudao.findAll();
		for (Grupo gru : grupos) {
			System.out.println(gru);
		}

		// buscar byexample
		System.err.println("Listar por findbyexample");
		grupos = grudao.findByExample(new Grupo("hardware"));
		for (Grupo gru : grupos) {
			System.out.println(gru);
		}

		grudao.cerrar();
	}

	private static void testVendedorDAO() throws SQLException {
		VendedorDAO venddao = new VendedorDAO();

		// insertar
		Vendedor v = new Vendedor("vendedor " + TUNOMBRE, LocalDate.of(2000, 8, 20), 1000);
		if ((v = venddao.insertGenKey(v)) != null) {
			System.err.println("Insertado!! " + v);
		}

		// listar
		List<Vendedor> vendedores = venddao.findAll();
		for (Vendedor vend : vendedores) {
			System.out.println(vend);
		}

		// buscar byexample
		System.err.println("Listar por findbyexample");
		vendedores = venddao.findByExample(new Vendedor(TUNOMBRE, null));
		for (Vendedor vend : vendedores) {
			System.out.println(vend);
		}
		System.out.println("--");
		vendedores = venddao.findByExample(new Vendedor(null, LocalDate.of(2000, 8, 20)));
		for (Vendedor vend : vendedores) {
			System.out.println(vend);
		}

		venddao.cerrar();
	}

	private static void testClienteDAO() throws SQLException {
		ClienteDAO clidao = new ClienteDAO();

		// insertar
		Cliente c = new Cliente("cliente " + TUNOMBRE, "direccion...");
		if ((c = clidao.insertGenKey(c)) != null) {
			System.err.println("Insertado!! " + c);
		}

		// listar
		List<Cliente> clientes = clidao.findAll();
		for (Cliente cli : clientes) {
			System.out.println(cli);
		}

		// buscar byexample
		System.err.println("Listar por findbyexample");
		clientes = clidao.findByExample(new Cliente(null, "Felde"));
		for (Cliente cli : clientes) {
			System.out.println(cli);
		}
		System.out.println("--");
		clientes = clidao.findByExample(new Cliente(TUNOMBRE, null));
		for (Cliente cli : clientes) {
			System.out.println(cli);
		}

		clidao.cerrar();
	}

}
