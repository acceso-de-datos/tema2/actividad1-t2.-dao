package dao;

import java.sql.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;
import java.time.ZoneId;

import modelo.Cliente;
import modelo.Grupo;
import modelo.Vendedor;



public class VendedorDAO implements GenericDAO<Vendedor>  {
	final String SQLSELECTALL = "SELECT * FROM vendedores";
	final String SQLSELECTPK = "SELECT * FROM vendedores WHERE id = ?";
	final String SQLINSERT = "INSERT INTO vendedores (nombre, fecha_ingreso, salario) VALUES (?, ?, ?)";
	final String SQLUPDATE = "UPDATE vendedores SET nombre = ?, fecha_ingreso = ?, salario = ? WHERE id = ?";
	final String SQLDELETE = "DELETE FROM vendedores WHERE id = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private PreparedStatement pstSelect;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstInsertGenKey;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;
	
	public VendedorDAO() throws SQLException{
		Connection con = ConexionBD.getConexion();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstInsert = con.prepareStatement(SQLINSERT);
		pstInsertGenKey =con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}
	
	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	@Override
	public Vendedor findByPK(int id) throws SQLException {
		Vendedor v = null;
		
		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();

		if (rs.first()) {
			return new Vendedor(id,rs.getString("nombre"), rs.getDate("fecha_ingreso").toLocalDate(), rs.getFloat("salario"));
		}
		rs.close();
		return v;
	}

	@Override
	public List<Vendedor> findAll() throws SQLException {
		List<Vendedor> listaVendedores = new ArrayList<Vendedor>();
		ResultSet rs = pstSelectAll.executeQuery();
		LocalDate date;
		while (rs.next()) {
			 
			listaVendedores.add(new Vendedor(rs.getInt("id"),rs.getString("nombre"), rs.getDate("fecha_ingreso").toLocalDate(), rs.getFloat("salario")));
		}
		rs.close();
		return listaVendedores;
	}

	@Override
	public List<Vendedor> findByExample(Vendedor vend) throws SQLException {
		String sql = "";

		Connection con = ConexionBD.getConexion();
		List<Vendedor>listaVendedores = new ArrayList<Vendedor>();
		if(vend.getNombre()!=null){
			sql="SELECT * FROM vendedores WHERE nombre like '%"+vend.getNombre()+"%'";

		}
		if(vend.getFecha_ingreso()!=null){
			sql="SELECT * FROM vendedores WHERE fecha_ingreso like '%"+vend.getFecha_ingreso()+"%'";

		}
		if(vend.getSalario()>0){
			sql="SELECT * FROM vendedores WHERE salario like '%"+vend.getSalario()+"%'";
		}

		if(vend.getFecha_ingreso()!=null&&vend.getNombre()!=null){
			sql="SELECT * FROM vendedores WHERE salario like '%"+vend.getSalario()+"%' AND nombre like '%"+vend.getNombre()+"%'";
		}
		if(vend.getFecha_ingreso()!=null&&vend.getNombre()!=null&&vend.getSalario()>0){
			sql="SELECT * FROM vendedores WHERE salario like '%"+vend.getSalario()+"%' AND nombre like '%"+vend.getNombre()+"%' AND fecha_ingreso like '%"+vend.getFecha_ingreso()+"%";

		}

		pstSelect=con.prepareStatement(sql);
		ResultSet rs = pstSelect.executeQuery();
		if (rs.first()) {
			listaVendedores.add(new Vendedor(rs.getInt("id"), rs.getString("nombre"), rs.getDate("fecha_ingreso").toLocalDate(), rs.getFloat("Salario")));
		}

		return listaVendedores;
	}

	@Override
	public boolean insert(Vendedor vend) throws SQLException {
		
		pstInsert.setString(1, vend.getNombre());
		pstInsert.setDate(2, java.sql.Date.valueOf(vend.getFecha_ingreso()));
		pstInsert.setFloat(3, vend.getSalario());
		int insertados = pstInsert.executeUpdate();
		return (insertados == 1);
	}

	@Override
	public Vendedor insertGenKey(Vendedor vend) throws SQLException {
		Vendedor vende = null;;
		pstInsertGenKey.setString(1, vend.getNombre());
		pstInsertGenKey.setDate(2, java.sql.Date.valueOf(vend.getFecha_ingreso()));
		pstInsertGenKey.setFloat(3, vend.getSalario());
		int insertados = pstInsertGenKey.executeUpdate();
		if (insertados==1){
			ResultSet rsClaves = pstInsertGenKey.getGeneratedKeys();
			rsClaves.first();

			vende= new Vendedor(rsClaves.getInt(1),vend.getNombre(),vend.getFecha_ingreso(),vend.getSalario());
			return vende;
		}
		return vende ;
	}

	@Override
	public boolean update(Vendedor vend) throws SQLException {
	
		pstUpdate.setString(1, vend.getNombre());
		pstUpdate.setDate(2, java.sql.Date.valueOf(vend.getFecha_ingreso()));
		pstUpdate.setFloat(3, vend.getSalario());
		pstUpdate.setInt(4, vend.getId());
			int insertados = pstUpdate.executeUpdate();
			return (insertados == 1);
	}

	@Override
	public boolean delete(int id) throws SQLException {
		pstDelete.setInt(1, id);
		int borrados = pstDelete.executeUpdate();
		return (borrados == 1);
	}

	@Override
	public boolean delete(Vendedor vend) throws SQLException {
		return this.delete(vend.getId());
	}

}
