package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modelo.Articulo;
import modelo.Cliente;
import modelo.Grupo;

public class GrupoDAO implements GenericDAO<Grupo> {
	final String SQLSELECTALL = "SELECT * FROM grupos";
	final String SQLSELECTPK = "SELECT * FROM grupos WHERE id = ?";
	final String SQLINSERT = "INSERT INTO grupos (descripcion) VALUES (?)";
	final String SQLUPDATE = "UPDATE grupos SET descripcion = ? WHERE id = ?";
	final String SQLDELETE = "DELETE FROM grupos WHERE id = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private PreparedStatement pstSelect ;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstInsertGenKey;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;
	
	public GrupoDAO() throws SQLException {
		Connection con = ConexionBD.getConexion();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstInsert = con.prepareStatement(SQLINSERT);
		pstInsertGenKey =con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}
	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	@Override
	public Grupo findByPK(int id) throws SQLException {
		Grupo gr = null;

		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();
		if (rs.first()) {
			return new Grupo(id,rs.getString("descripcion"));
		}
		rs.close();
		return gr;
	
	}

	@Override
	public List<Grupo> findAll() throws SQLException {
		List<Grupo> listaGrupo = new ArrayList<Grupo>();
		ResultSet rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaGrupo.add(new Grupo(rs.getInt("id"),rs.getString("descripcion")));
		}
		rs.close();
		return listaGrupo;
	}

	@Override
	public List<Grupo> findByExample(Grupo grup) throws SQLException {
		String sql = "";
		Connection con = ConexionBD.getConexion();
		List<Grupo>listaGrupos = new ArrayList<Grupo>();
		if(grup.getDescripcion()!=null){
			sql="SELECT * FROM grupos WHERE descripcion like '%"+grup.getDescripcion()+"%'";
		}

		pstSelect=con.prepareStatement(sql);
		ResultSet rs = pstSelect.executeQuery();
		if (rs.first()) {
			listaGrupos.add(new Grupo(rs.getInt("id"), rs.getString("descripcion")));
		}

		return listaGrupos;
	}

	@Override
	public boolean insert(Grupo grup) throws SQLException {
		pstInsert.setString(1, grup.getDescripcion());
		int insertados = pstInsert.executeUpdate();
		return (insertados == 1);
	}

	@Override
	public Grupo insertGenKey(Grupo grup) throws SQLException {
		Grupo gr = null;;
		pstInsertGenKey.setString(1, grup.getDescripcion());
		int insertados = pstInsertGenKey.executeUpdate();
		if (insertados==1){
			ResultSet rsClaves = pstInsertGenKey.getGeneratedKeys();
			rsClaves.first();

			gr= new Grupo(rsClaves.getInt(1),grup.getDescripcion());
			return gr;
		}
		return gr ;
	}

	@Override
	public boolean update(Grupo grup) throws SQLException {
		pstUpdate.setString(1, grup.getDescripcion());
		pstUpdate.setInt(2, grup.getId());
		int insertados = pstInsert.executeUpdate();
		return (insertados == 1);
	}

	@Override
	public boolean delete(int id) throws SQLException {
		pstDelete.setInt(1, id);
		int borrados = pstDelete.executeUpdate();
		return (borrados == 1);
	}

	@Override
	public boolean delete(Grupo grup) throws SQLException {
		return this.delete(grup.getId());
		
	}

}
